Change Log - webap fuse
=============================

# [Unreleased](https://bitbucket.org/rapi3/webapp-fuse/commits/all)

[Full Changelog](hhttps://bitbucket.org/rapi3/webapp-fuse/branches/compare/v1.0.0%0Dmaster)

## Broken

## Bug Fixes:

## Features:

# [1.0.4](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v1.0.4) (2017-05-22)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v1.0.4%0Dmaster#diff)

## Bug Fixes:
- **rfInputCity** fix autocomplete placeholder
- **Auth** fix event auth

## Features:
- **report** create new componets: rf-report-sidenav | rf-report-box | rf-report-actions
- **w3config** custom Credits
- **suporte** freshdesk integrate suporte
- **XSession** new componet session X request Header
- **Metas** Create Helper Metas


# [1.0.3](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v1.0.3) (2017-03-14)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v1.0.3%0Dmaster#diff)

## Features:
- **ngMask** new format mask RG
- **report** create module rapi.fuse.report

# [0.3.2](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.3.2) (2017-03-01)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.3.2%0Dmaster#diff)

## Bug Fixes:
- **City** correção da rota de searchCEP

## Features:
- **rfInputDateBr** new directive (Gera input text com mascara de data BR)

# [0.3.1](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.3.1) (2017-02-28)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.3.1%0Dmaster#diff)

## Features:
- **City** novo methodo `searchCEP`
- **Sidebar navigation** show version
- **RRule** create lib de gerar RRULE date
- **$rootScope.fmask** Contants masks
- **suporte** new template


# [0.3.0](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.0.3) (2017-02-01)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.0.3%0Dmaster#diff)

## Features:
- **rfActivities** new directive
- **rfComments** new directive
- **rfInputCity** new directive


# [0.1.3](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.1.3) (2017-01-29)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.1.3%0Dmaster#diff)

#Broken
- **w3config** move rapi.fuse.config para plugin-w3

## Features:
- **Route** Create Route abstract => popup

# [0.1.0](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.1.0) (2016-12-08)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.1.0%0Dmaster#diff)

#Broken
- **rapi.fuse.w3** remove from source, use depence https://bitbucket.org/rapi3/webapp-w3

## Bug Fixes:
- **w3-upload** fix src image icon upload


# [0.0.2](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.0.2) (2016-10-23)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.0.2%0Dmaster#diff)

## Bug Fixes:
- **w3InputCity** diretive w3InputCity fiz set model where is NULL

## Features:
- **w3Avatar** new attribute w3-presenter
- **w3Presenter** Add paramenter index in presenter
- **w3Auth** store user e store profile on login
- **w3Resouce** suporte a w3Presenter in request e response


# [0.0.1](https://bitbucket.org/rapi3/webapp-fuse/commits/tag/v0.0.1) (2016-09-09)
[Full Changelog](https://bitbucket.org/rapi3/webapp-fuse/branches/compare/v0.0.1%0Dmaster#diff)
> foi copiado esse puglin de  => https://bitbucket.org/rapi3/fuse/src/95a523a721a5e9a100f5550d5b0f57f01f3ef071/resources?at=v0.1.1

## Features:
- **w3ApiUtils** new service w3ApiUtils with action searchCEP
- **w3Acitivity** create directive
- **w3Utils** create method  w3Utils.confirm
- **w3Reload** create HttpIntecptorn from config htp w3Reload (use ng-hide="$root.w3Reload['someKey']" or ng-hide="$root.loadingProgress")
- **w3Comment** create dirctive w3-comment e service RapiComment
- **w3Avatar** create dirctive w3-avatar
- **w3Config** create auth.onLoginRequired (agora deve ser passada uma closure para tratamento de login requerido)
- **w3Auth** create service
- **w3HttInteceptor** add suporte custon tokenGetter
- **toobar** logout e show name profile
- **params route** Create option state to URL external
- **w3Presenter** create service
