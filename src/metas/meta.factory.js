(function () {
    'use strict';

    angular
            .module('rapi.fuse.metas')
            .factory('Meta', MetaService);


    /** @ngInject */
    function MetaService(w3Resource) {

        var urlApi = '/rapi/metas';
//        var Presenter = w3Presenter.make(null, requestPresenter);

        return w3Resource.make(urlApi);
        //return w3Resource.make(urlApi, Presenter);
        //return w3Resource.extend(urlApi, service);

        //----------------------------------
            
    }

})();
