/* global _ */

(function () {
    'use strict';

    angular
            .module('rapi.fuse.metas')
            .factory('MetaHelper', MetaHelperFactory)
            .factory('MetaLoadHelper', MetaLoadHelperFactory);


    /** @ngInject */
    function MetaHelperFactory($filter, Meta)
    {
        function Config(ownerType, ownerId, metas)
        {
            metas = metas || [];
            this.ownerType = ownerType;
            this.ownerId = ownerId;
            this.metas = this.transforms(metas);
        }

        Config.prototype.search = function (params)
        {
            return $filter('filter')(this.metas, params) || [];
        };

        Config.prototype.filterByType = function (type)
        {
            return this.search({'$metaType': type});
        };

        Config.prototype.filterByKey = function (key)
        {
            return this.search({'$metaKey': key});
        };

        Config.prototype.transforms = function (values)
        {
            var self = this;

            return _.map(values, function (d) {
                return self.item(d);
            });
        };

        Config.prototype.item = function (meta)
        {
            return new MagicMeta(meta, this);
        };

        Config.prototype.first = function (key)
        {
            var result = this.filterByKey(key);
            return result.length ? result[0] : null;
        };

        /**
         * Igual ao this.fisrt porém se ele nao encontrar
         * ele cria um obj <MagicMeta> temporario
         */
        Config.prototype.fetch = function (key, type)
        {
            var result = this.filterByKey(key);

            if (result.length) {
                return result[0];
            }

            //Se ele nao encontrar vai criar um meta sem ID temporario
            return this.new(key, type);
        };

        Config.prototype.new = function (key, type, value)
        {
            var meta = {
                meta_key: key,
                meta_value: value || null,
                meta_type: type || key
            };

            return this.item(meta);
        };

        /**
         * Salva uma meta ja transformada em <MagicMeta>
         *
         * @param {MagicMeta} magicMeta
         * @returns {promisse}
         */
        Config.prototype.save = function (magicMeta)
        {
            var promisse;

            if (magicMeta.$metaId) {
                promisse = this.update(magicMeta.$metaId, magicMeta.$toModel());
            } else {
                promisse = this.create(magicMeta.$metaKey, magicMeta.$getValue(), magicMeta.$metaType);
            }

            return promisse;
        };

        Config.prototype.update = function (id, data)
        {
            return Meta.update(id, data);
        };

        Config.prototype.create = function (key, value, type)
        {
            var self = this;

            var obj = {
                'meta_key': key,
                'meta_value': value,
                'meta_type': type || null,
                'owner_type': this.ownerType,
                'owner_id': this.ownerId
            };

            return Meta.save(obj).then(function (result) {
                self.metas.push(self.item(result));
                return result;
            });
        };

        Config.prototype.remove = function (id)
        {
            var self = this;

            return Meta.remove(id).then(function () {

                angular.forEach(self.metas, function (v, i) {
                    if (v.$metaId === id) {
                        self.metas.splice(i, 1);
                    }
                });

            });
        };

        /**
         * Deleta todas Metas com a valor da KEY
         *
         * @param string key
         * @returns void
         */
        Config.prototype.removeByKey = function (key)
        {
            var self = this;
            var results = this.filterByKey(key);

            angular.forEach(results, function (meta) {
                self.remove(meta.$metaId);
            });
        };

        /**
         *
         * @param {type} meta
         * @param {type} helper
         * @returns {meta_helper.factoryL#3.MetaHelperFactory.MagicMeta}
         *
         * ========================================================================
         */
        function MagicMeta(meta, helper)
        {
            this.$helper = helper;
            this.$array = [];

            this.$metaId = null;
            this.$metaKey = null;
            this.$metaType = null;
            this.$metaValue = null;

            this.$extend(meta);

            return this;
        }

        MagicMeta.prototype = {
            $extend: function (meta)
            {
                this.$metaId = meta.id;
                this.$metaKey = meta.meta_key;
                this.$metaType = meta.meta_type;
                this.$metaValue = meta.meta_value;

                if (!meta.meta_value) {
                    this.$metaValue = {};
                } else if (angular.isArray(this.$metaValue)) {
                    this.$toArray();
                } else {
                    this.$toObject();
                }

            },
            $save: function ()
            {
                var self = this;

                return this.$helper.save(self).then(function (result) {
                    self.$extend(result);
                });
            },
            $remove: function ()
            {
                return this.$helper.remove(this.$metaId);
            },
            $toObject: function ()
            {
                for (var name in this.$metaValue) {
                    this[name] = this.$metaValue[name];
                }
                return this;
            },
            $toArray: function ()
            {

                var self = this;
                this.$array = [];

                angular.forEach(this.$metaValue, function (item) {
                    self.$push(item);
                });
                return this;
            },
            $push: function (item)
            {
                //this.$array.push(item);
                var i = this.$array.push();
                this.$array[i] = MagicArray.call(item, i, this);
                return this;
            },
            $toModel: function ()
            {
                var model = {
                    id: this.$metaId,
                    meta_key: this.$metaKey,
                    meta_value: this.$getValue(),
                    meta_type: this.$metaType
                };


                return model;
            },
            $getValue: function ()
            {
                var data = {};

                if (this.$array.length) {
                    data = this.$array;
                } else {

                    for (var name in this) {
                        if (name.substr('0', 1) !== '$')
                            data[name] = this[name];
                    }

                }


                return data;
                //return this.$array.length ? this.$array : this.$metaValue;
            }
        };

        /**
         * @param {array} data
         * @param {MagicMeta} magiMeta
         * @returns {undefined}
         */
        function MagicArray($index, magiMeta)
        {
            this.$removeData = function () {
                magiMeta.$array.splice($index, 1);
                magiMeta.$save();
            };

            this.$saveData = function (data) {
                magiMeta.$array[$index] = data;
                magiMeta.$save();
            };

            this.$updateData = function (data) {
                magiMeta.$array[$index] = angular.extend(magiMeta.$array[$index], data);
                magiMeta.$save();
            };

            return this;
        }



//        MagicMeta.prototype.$save =  function()
//        {
//            this.helper.updateOrCreate(this.$meta_key, this, this.$meta_type);
//        };
//
//        MagicMeta.prototype.getData =  function()
//        {
//            var result = {};
//
//            angular.forEach(function(){
//
//            });
//        };

        function construct(ownerType, ownerId, metas)
        {
            return new Config(ownerType, ownerId, metas);
        }



        return construct;
    }

    /** @ngInject */
    function MetaLoadHelperFactory(Meta, MetaHelper)
    {

        function construct(ownerType, ownerId)
        {
            var data = {
                owner_id: ownerId,
                owner_type: ownerType
            };
            
            return Meta.search(data).then(function (metas) {
                return new MetaHelper(ownerType, ownerId, metas);
            });
        }

        return construct;
        
    }

})();
