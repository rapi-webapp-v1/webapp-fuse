(function ()
{
    'use strict';

    angular
            .module('rapi.fuse.logs', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider)
    {
        $stateProvider

                .state('app.logs', {
                    url: '/logs',
                    views: {
                        'content@app': {
                            templateUrl: 'app/external/fuse/logs/logs-archive.html',
                            controller: 'LogsController as vm'
                        }
                    }
                });

        //            Deve ser adicionado na mão
        //            msNavigationServiceProvider.saveItem('logs', {
        //              title: 'CITIES.MENU',
        //              icon: 'icon-city',
        //              state: 'app.cities.tabbed',
        //              weight: 15
        //            });
    }
})();
