(function() {
  'use strict';

  angular
    .module('rapi.fuse.logs')
    .controller('logsDialogController', logsDialogController);

  /** @ngInject */
  function logsDialogController($mdDialog, row) {

    var vm = this;

    //Data
    vm.row   = {};

    //Methods
    vm.closeDialog = closeDialog;

    activate();
    //-----------------------------------

    function activate() {
      vm.row   = angular.copy(row);
            
    }


    function closeDialog(params) {
      $mdDialog.hide(params);
    }

  }


})();
