(function() {
  'use strict';

  angular
    .module('rapi.fuse.logs')
    .factory('Log', LogService);


  /** @ngInject */
  function LogService(w3Resource) {

    var urlApi  = '/rapi/activities/all';

    var service = {
    };

    return w3Resource.extend(urlApi, service);

    // ///////////////////////////////


  }

})();
