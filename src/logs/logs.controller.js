(function () {
    'use strict';

    angular
            .module('rapi.fuse.logs')
            .controller('LogsController', LogsController);

    /** @ngInject */
    function LogsController( $mdDialog, Log) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'created_at', //EX: nome,-created_at
            'q': null,
            'include' : 'causer'
        };

        // Methods
        vm.showDetail = showDetail;
        vm.search = search;
        vm.setPage = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Log.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Log.pagination;
            });

        }

        function setPage(p)
        {
            Log.setPage(p);
            activate();
        }

        function search()
        {
            Log.setPage(1);
            activate();
        }



        function showDetail(row, ev) {
            $mdDialog.show({
                controller: 'logsDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/external/fuse/logs/dialogs/logs-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row
                }

            });
        }


    }


})();
