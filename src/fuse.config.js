(function ()
{
    'use strict';

    angular
            .module('rapi.fuse')
            .config(config);

    /** @ngInject */
    function config($translateProvider)
    {
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });
        
        $translateProvider.preferredLanguage('pt-br');
        $translateProvider.useSanitizeValueStrategy('sanitize');
    }

})();
