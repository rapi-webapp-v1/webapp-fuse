(function () {
    'use strict';

    angular
            .module('rapi.fuse.report')
            .factory('Report', ReportService);


    /** @ngInject */
    function ReportService(w3Config, w3Auth, w3Date) {

        var service = {
            newQuery: newQueryBuilder
        };

        return service;
        //==============================
        function newQueryBuilder() {
            return new Report(w3Date, w3Config.getConfigHost().baseUrl, w3Auth);
        }
    }

    function Report(w3Date, baseUrl, w3Auth)
    {
        this.w3Date = w3Date;
        this.baseUrl = baseUrl;
        this.token = null;
        this.requiredCols = ['report'];
        this.onCols = [];
        this.onMultiplesValue = [];
        this.$invalid = true;

        this.params = {
            'format': 'html', //html/pdf/csv/xls
            'ouput': 'view', //view/download/email/print
            'report': null
        };

        this.autoToken = function()
        {
            var self = this;
            return w3Auth.token().then(function(tk){
                self.token = tk;
            });
        };

    }

    Report.prototype.validate = function () {
        var self = this;
        var invalid = false;

        this.requiredCols.map(function (req) {
            if (!self.check(req)) {
                invalid = true;
            }
        });

        this.$invalid = invalid;
        return invalid;
    };

    Report.prototype.setOnMultiplesValues = function (cols)
    {
        this.onMultiplesValue = cols;
    };

    Report.prototype.isMulti = function (cols)
    {
        if (this.onMultiplesValue.indexOf(cols) === -1) {
            return 1;
        }
    };

    Report.prototype.setOnColumns = function (cols)
    {
        this.onCols = cols;
        this.validate();
    };

    Report.prototype.setRequiredColumns = function (cols)
    {
        this.requiredCols = cols;
        this.requiredCols.push('report');
        this.validate();
    };

    Report.prototype.off = function (col)
    {
        return !this.on(col);
    };

    Report.prototype.on = function (col)
    {
        return (this.onCols.indexOf(col) !== -1);
    };

    Report.prototype.check = function (key)
    {
        return (this.params[key]) ? true : false;
    };

    Report.prototype.required = function (col)
    {
        return (this.requiredCols.indexOf(col) !== -1);
    };

    Report.prototype.setReport = function (repo)
    {
        this.setParam('report', repo);
    };

    Report.prototype.setFormat = function (format)
    {
        this.setParam('format', format);
    };

    Report.prototype.setOutput = function (out)
    {
        this.setParam('ouput', out);
    };

    Report.prototype.setDate = function (key, date)
    {
        this.setParam(key, this.w3Date.toFormatEn(date));
    };

    Report.prototype.setParam = function (key, value)
    {
        this.params[key] = value;
        this.validate();
    };

    Report.prototype.setBetweenDate = function ($dtParams)
    {
        this.setParam('dt_column', $dtParams.dt_column);
        this.setDate('dt_start', $dtParams.dt_start);
        this.setDate('dt_end', $dtParams.dt_end);
    };

    Report.prototype.pluck = function (key, colection)
    {
        var selecteds;
        if (!angular.isArray(colection))
            return null;

        selecteds = colection.map(function (row) {
            return row.id;
        }).join("|");
        
        this.setParam(key, selecteds);
    };

    /**
     * use para arrays simples formados por strings ou números ex: ['pendente']
     * @param {type} key
     * @param {type} colection
     * @returns {unresolved}
     */
    Report.prototype.simplPluck = function (key, colection)
    {
        var selecteds;
        if (!angular.isArray(colection))
            return null;

        selecteds = colection.map(function (row) {
            return row;
        }).join("|");
        
        this.setParam(key, selecteds);
    };

    Report.prototype.url = function ()
    {
        var params = this.params;
        params.token = this.token;
        var q = $.param(params);
        
        var url = this.baseUrl + "/rapi/report/render";
        return url + "?" + q;
    };

    Report.prototype.open = function ()
    {
        //console.log('OPEN', this.params, this.url());
        var self = this;

        this.autoToken().then(function(){
            window.open(self.url());
        });
        //window.open(url, "relatoriossifra", "height=800,width=1000");
    };

    Report.prototype.sendReport = function (format, output)
    {       
        output = output || 'view';
        this.setFormat(format);
        this.setOutput(output);
        this.open();
    };

    Report.prototype.transformChip = function (chip)
    {
        if (angular.isObject(chip)) {
            return chip;
        }
        return {
            name: chip
        };
    };


})();
