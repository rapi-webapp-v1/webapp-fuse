(function () {
    angular.module('rapi.fuse.report')
            .directive('rfReportBox', reportBoxDirective);

    /**
     * <rf-report-box rf-report=" " ></rf-report-box>
     * 
     */
    /** @ngInject */
    function reportBoxDirective()
    {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/external/fuse/report/box/box.html',
            replace: true,
            transclude: true,
            scope: {
                report: '=rfReport'
            }
        };
        return directive;
    }



})();