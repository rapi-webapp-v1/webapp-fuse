(function () {
    angular.module('rapi.fuse.report')
            .controller('reportActionsController', reportActionsController)
            .directive('rfReportActions', reportActionsDirective);

    /**
     * <rf-report-actions rfreport="" rf-disable=" rfaction="" ></rf-report-actions>
     * 
     */
    /** @ngInject */
    function reportActionsDirective()
    {

        var directive = {
            restrict: 'E',
            replace: false,
            templateUrl: 'app/external/fuse/report/actions/actions.html',
            transclude: true,
            controller: 'reportActionsController as rp',
            scope: {
                report: '=rfreport',
                disable: '<rfDisable',
                rfaction: '&rfaction'
            }

        };
        return directive;


    }

    /** @ngInject */
    function reportActionsController($scope)
    {
        //DATA
        var rp = this;
   
        //METHODS
        rp.execute = execute;

        //-----------------------
        
   
        
        function execute(format)
        {
            $scope.rfaction({'$format': format});
        }
     
    }



})();