(function () {
    angular.module('rapi.fuse.report')
            .controller('reportSidenavController', reportSidenavController)
            .directive('rfReportSidenav', reportSidnavDirective);

    /**
     * <rf-report-sidenav rf-list="" ></rf-report-sidenav>
     * 
     */
    /** @ngInject */
    function reportSidnavDirective()
    {

        var directive = {
//            link: link,
            restrict: 'E',
            replace: false,
            templateUrl: 'app/external/fuse/report/sidenav/sidenav.html',
            scope: {
                list: '=rfList',
                onChange : '&'
            },
            controller: 'reportSidenavController as rp'

        };
        return directive;


    }


    /** @ngInject */
    function reportSidenavController($scope)
    {
        //DATA
        var rp = this;


        //METHODS
        rp.setParams = setParams;

        //--------------------

        function setParams(item)
        {   
            $scope.onChange({'$report':item});
        }
    }

})();