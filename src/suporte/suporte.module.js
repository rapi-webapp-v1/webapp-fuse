(function ()
{
    'use strict';

    angular
            .module('rapi.fuse.suporte', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider)
    {

        // State
        $stateProvider
                .state('app.suporte', {
                    url: '/suporte',
                    views: {
                        'content@app': {
                            templateUrl: 'app/external/fuse/suporte/suporte.html'
                        }
                    }
                })
                ;
        msNavigationServiceProvider.saveItem('suporte', {
            title: 'Suporte do sistema',
            icon: 'icon-account-multiple',
            state: 'app.suporte',
            weight: 99
        });


    }
})();


