(function ()
{
    'use strict';

    angular
            .module('rapi.fuse.profile', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
                .state('app.profile', {
                    url: '/perfil',
                    views: {
                        'content@app': {
                            templateUrl: 'app/external/fuse/profile/profile.html',
                            controller: 'ProfileController as vm'
                        }
                    }
                });


        msNavigationServiceProvider.saveItem('profile', {
            title: 'PROFILE.MENU',
            icon: 'icon-bank',
            state: 'app.profile',
            weight: 100
        });

    }
})();
