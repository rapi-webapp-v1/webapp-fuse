(function () {
    'use strict';

    angular
            .module('rapi.fuse.profile')
            .controller('ProfileController', ProfileController);

    /** @ngInject */
    function ProfileController() {
        var vm = this;
        
        //DATA
        vm.profile = {};
        
        // METHOS
        
        activate();
        
        //----------------------
        function activate() 
        {
            vm.profile = {
                name: 'Maiara',
                email: 'maiara@make.pro.br',
                profile: {
                    sex: 'F',
                    dt_nascimento: '12/12/2017',
                    name: 'Maiara Aparecida Sassi Cristan'
                },
                created_at: '12/12/2016'
            };
        }
    };
})();
