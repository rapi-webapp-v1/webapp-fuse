(function () {
    'use strict';

    angular
            .module('rapi.fuse.cities')
            .factory('City', cidadeService);


    /** @ngInject */
    function cidadeService($mdDialog, w3Resource, $http, w3Utils) {

        var urlApi = '/rapi/cities';

        var service = {
            searchCEP: searchCEP,
            dialog: {
                create: showCreateDialog,
                edit: showDialog
            }
        };

        return w3Resource.extend(urlApi, service);

        // ///////////////////////////////

        // DIALOGS
        /** archive: array com a lista de itens */
        function showCreateDialog(archive, ev, data) {
            return showDialog(data, archive, ev);
        }

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(row, archive, ev) {

            return $mdDialog.show({
                controller: 'CityDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/external/fuse/cities/cities/cities-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': archive
                }

            });

        }

        function searchCEP(cep) {

            var data = {
                params: {
                    cep: cep,
                    include: 'city.state.country'
                }
            };

            return $http.get(w3Utils.urlApi(urlApi + "/cep"), data).then(function (result) {
                return result.data.data;
            });
        }
    }

})();
