(function() {
  'use strict';

  angular
    .module('rapi.fuse.cities')
    .controller('CityDialogController', CityDialogController);

  /** @ngInject */
  function CityDialogController($mdDialog, City,Estado, row, archive, Pais) {

    var vm = this;

    //Data
    vm.isNew = true;
    vm.row   = angular.copy(row);
    vm.title = null;
    vm.states = [];
    vm.countrys = [];

    //Methods
    vm.closeDialog = closeDialog;
    vm.update = update;
    vm.save = save;
    vm.loadCountry = loadCountry;
    vm.loadState = loadState;

    activate();
    //-----------------------------------

    function activate() {
      archive = archive || [];
        if (vm.row && vm.row.id) {
            vm.isNew = false;
        }
        if(!vm.isNew){
          vm.row.country_id = vm.row.state.country_id;
          loadCountry();
          loadState(vm.row.country_id);
        }
    }

    function update() {
      City.update(row.id, vm.row).then(function(result) {
        if (result) {
          angular.extend(row, result);

          closeDialog(row);
        }
      });
    }

    function save() {
      vm.row.include='state.country';
      City.save(vm.row).then(function(result) {
        if (result) {
          archive.push(result);
          closeDialog(result);
        }
      });
    }

    function closeDialog(params) {
      $mdDialog.hide(params);
    }

    function loadState(country_id)
    {

      Estado.search({'country_id':country_id}).then(function (result) {
                vm.states = result;
            });
    }
    function loadCountry()
    {
        var params = {
            page:null,
            paginate:null,
            sort:'country_name'
        }
      Pais.search(params).then(function (result) {
                vm.countrys = result;
            });
    }

  }


})();
