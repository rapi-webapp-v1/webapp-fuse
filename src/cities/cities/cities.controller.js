(function () {
    'use strict';

    angular
            .module('rapi.fuse.cities')
            .controller('CityController', CityController);

    /** @ngInject */
    function CityController(City, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'city_name', //EX: nome,-created_at
            'q' : null,
            'include' : 'state.country'
        };

        // Methods
        vm.showCreate    = showCreate;
        vm.showEdit      = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {            
            vm.loading = true;
            vm.archive = null;

            City.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = City.pagination;
            });

        }

        function setPage(p)
        {
            City.setPage(p);
            activate();
        }

        function search()
        {
            City.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            City.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            City.dialog.edit(row, vm.archive, ev);
        }

       function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('City.MSG_CONFIRM_DELETE'), item.nome, $event)
                    .then(function () {
                        City.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
