(function() {
  'use strict';

  angular
    .module('rapi.fuse.cities')
    .factory('Estado', estadoService);


  /** @ngInject */
  function estadoService($mdDialog, w3Resource) {

    var urlApi  = '/rapi/states';

    var service = {
      dialog : {
        create: showCreateDialog,
        edit  : showDialog
      }
    };

    return w3Resource.extend(urlApi, service);

    // ///////////////////////////////

    // DIALOGS
    /** archive: array com a lista de itens */
    function showCreateDialog( archive, ev){
      return showDialog(null, archive, ev);
    }

    /** Abre um Dialog para cadastr/edição de um item
     * row: objeto item que está sendo editado
     *  archive: array com a lista de itens
     */
    function showDialog(row, archive, ev){

      return $mdDialog.show({
        controller: 'StatesDialogController',
        controllerAs: 'vm',
        templateUrl: 'app/external/fuse/cities/states/states-dialog.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          'row': row,
          'archive': archive
        }

      });

    }

  }

})();
