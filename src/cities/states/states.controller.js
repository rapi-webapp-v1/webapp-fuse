(function () {
    'use strict';

    angular
            .module('rapi.fuse.cities')
            .controller('StateController', StateController);

    /** @ngInject */
    function StateController(Estado, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.paises = [];
        vm.params = {
            'paginate': 10,
            'sort': 'state_name', //EX: nome,-created_at
            'q' : null,
            'include' : 'country'
        };

        // Methods
        vm.showCreate    = showCreate;
        vm.showEdit      = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Estado.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Estado.pagination;
            });

        }

        function setPage(p)
        {
            Estado.setPage(p);
            activate();
        }

        function search()
        {
            Estado.setPage(1);
            activate();
        }


        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            Estado.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            Estado.dialog.edit(row, vm.archive, ev);
        }

       function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('CITIES.MSG_CONFIRM_DELETE'), item.nome, $event)
                    .then(function () {
                        Estado.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
