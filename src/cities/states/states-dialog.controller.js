(function() {
  'use strict';

  angular
    .module('rapi.fuse.cities')
    .controller('StatesDialogController', StatesDialogController);

  /** @ngInject */
  function StatesDialogController($mdDialog, Estado, row, archive, Pais) {

    var vm = this;

    //Data
    vm.isNew = true;
    vm.row = angular.copy(row);
    vm.title = null;
    vm.countries = [];

    //Methods
    vm.closeDialog = closeDialog;
    vm.update = update;
    vm.save = save;

    activate();
    //-----------------------------------

    function activate() {
      archive = archive || [];
      loadCountries();
      if (vm.row) {
        vm.isNew = false;
      }
    }

    function update() {
      Estado.update(row.id, vm.row).then(function(result) {
        if (result) {
          angular.extend(row, result);
          closeDialog(row);
        }
      });
    }

    function save() {
      Estado.save(vm.row).then(function(result) {
        if (result) {
          archive.push(result);
          closeDialog(result);
        }
      });
    }

    function closeDialog(params) {
      $mdDialog.hide(params);
    }


    function loadCountries() {

      Pais.search(vm.params).then(function(result) {
        vm.countries = result;
      });
    }

  }


})();
