(function () {
    'use strict';

    angular
            .module('rapi.fuse.cities')
            .controller('CountryController', CountryController);

    /** @ngInject */
    function CountryController(Pais, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'country_name', //EX: nome,-created_at
            'q' : null
        };

        // Methods
        vm.showCreate    = showCreate;
        vm.showEdit      = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Pais.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Pais.pagination;
                // console.log(vm.archive);
            });

        }

        function setPage(p)
        {
            Pais.setPage(p);
            activate();
        }

        function search()
        {
            Pais.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            Pais.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            Pais.dialog.edit(row, vm.archive, ev);
        }

       function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('CITIES.MSG_CONFIRM_DELETE'), item.nome, $event)
                    .then(function () {
                        Pais.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }



})();
