(function() {
  'use strict';

  angular
    .module('rapi.fuse.cities')
    .controller('CountryDialogController', CountryDialogController);

  /** @ngInject */
  function CountryDialogController($mdDialog, Pais, row, archive) {

    var vm = this;

    //Data
    vm.isNew = true;
    vm.row   = angular.copy(row);
    vm.title = null;

    //Methods
    vm.closeDialog = closeDialog;
    vm.update = update;
    vm.save = save;

    activate();
    //-----------------------------------

    function activate() {
      archive = archive || [];

        if (vm.row) {
            vm.isNew = false;
        }
    }

    function update() {
      Pais.update(row.id, vm.row).then(function(result) {
        if (result) {
          angular.extend(row, result);
          closeDialog(row);
        }
      });
    }

    function save() {
      Pais.save(vm.row).then(function(result) {
        if (result) {
          archive.push(result);
          closeDialog(result);
        }
      });
    }

    function closeDialog(params) {
      $mdDialog.hide(params);
    }

  }


})();
