(function ()
{
    'use strict';

    angular
            .module('rapi.fuse.cities', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider)
    {
        //mytodo Criar filtro buscar cidades por pais ou estados
        //mytodo Criar Diaog para selecionar o pais, depois os estados do pais selecionado e so entao o campo nome da cidades
        //mytodo Melhorar btns de cadastr cidades
        $stateProvider
                .state('app.cities', {
                    abstract: true,
                    url: '/cities',
                    bodyClass: 'cities'
                })
                .state('app.cities.tabbed', {
                    url: '/city',
                    views: {
                        'content@app': {
                            templateUrl: 'app/external/fuse/cities/tabbed.html'
                        },
                        'viewTabCity@app.cities.tabbed': {
                            templateUrl: 'app/external/fuse/cities/cities/cities-archive.html',
                            controller: 'CityController as vm'
                        },
                        'viewTabState@app.cities.tabbed': {
                            templateUrl: 'app/external/fuse/cities/states/states-archive.html',
                            controller: 'StateController as vm'
                        },
                        'viewTabCountry@app.cities.tabbed': {
                            templateUrl: 'app/external/fuse/cities/countrys/countrys-archive.html',
                            controller: 'CountryController as vm'
                        }
                    }
                });

                //            O menu pode ser adicionado manualmente
                //            msNavigationServiceProvider.saveItem('cidade', {
                //              title: 'CITIES.MENU',
                //              icon: 'icon-city',
                //              state: 'app.cities.tabbed',
                //              weight: 15
                //            });
    }
})();
