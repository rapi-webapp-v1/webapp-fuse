(function () {
    'use strict';

    angular
            .module('rapi.fuse.auth')
            .controller('AuthController', authController)
            .controller('LogoutController', logoutController)
            .controller('LoginTokenController', loginTokenController);

    /** @ngInject */
    function authController($scope, $rootScope) {

        // Remove the splash screen
        $scope.$on('$viewContentAnimationEnded', function (event) {
            if (event.targetScope.$id === $scope.$id) {
                $rootScope.$broadcast('msSplashScreen::remove');
            }
        });

    }

    /** @ngInject */
    function loginTokenController($stateParams, $state, w3Auth, w3Config) {

        var token = $stateParams.access_token;

        w3Auth.loginFromToken(token).then(function () {
            $state.go(w3Config.get('auth').routeSuccess);
        });

    }

    /** @ngInject */
    function logoutController($state, w3Auth) {

        w3Auth.logout().then(function () {
            $state.go('auth.login');
        });

    }


})();
