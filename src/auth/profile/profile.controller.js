(function ()
{
    'use strict';

    angular
        .module('rapi.fuse.auth')
        .controller('ProfileController', ProfileController);

    /** @ngInject */
    function ProfileController($http, w3Utils)
    {
        var vm = this;

        // Data
        vm.profile = {};
        vm.showForm = false;
        //Mytodo remove text marcação
        vm.about ={
            name: 'Maiara Ap. Sassi',
            genero: 'Feminino',
            dt_nasc: '08/08/1991',
            ocupacao: 'Gerente comercial',
            address: 'R. Cabo Alifalis de Paula Freitas, 529, Ap 06. Centro, Santa Terezinha de Itaipu - PR',
            phones: {phone1: '99218802', phone2: '3541-1523'},
            emails: {email1: 'atendimento@valmirbarbosa.com.br', email2: 'maiara.sassi@gmail.com'},
            thumb: 'https://lh3.googleusercontent.com/-y9XJfBLN6PM/AAAAAAAAAAI/AAAAAAAAACY/5uj0eLQT0I0/s180-c-k-no/photo.jpg'
        };

        vm.activities =[
            {
                assunto: 'Cadastro de produtos',
                message: 'Cadastrou um novo produto na categoria carros',
                time: '23/09/2016'
            },
            {
                assunto: 'Cadastro de empresa',
                message: 'Cadastrou uma nova empresa',
                time: '23/09/2016'
            },
            {
                assunto: 'Edição de cadastro',
                message: 'Editou a categoria de produtos "Sportivos"',
                time: '23/09/2016'
            }
        ];

        vm.nome = vm.about.name;
        vm.avatar = vm.about.thumb;

        // Methods
        vm.toggleEdit = toggleEdit;
        vm.salvar   = salvar;

        activate();
        //////////
        function activate()
        {
            //mytodo colocar isso dentro do service Auth
            $http.get(w3Utils.urlApi('/rapi/profile')).then(function(result) {
                vm.profile = result.data.data;
            });
        }

        function toggleEdit()
        {
            vm.showForm = !vm.showForm;

            if(vm.showForm){
                vm.row = vm.about;
            }
        }

        function salvar()
        {
            toggleEdit();
            alert('Salvou');
        }
    }

})();
