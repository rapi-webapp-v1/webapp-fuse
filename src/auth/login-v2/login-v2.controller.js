(function () {
    'use strict';

    angular
            .module('rapi.fuse.auth')
            .controller('LoginV2Controller', LoginV2Controller);

    /** @ngInject */
    function LoginV2Controller(w3Config, $state, w3Auth) {

        var vm = this;

        // Data
        vm.credentials = {
            include: 'profile',
            type: w3Config.get('auth').type
        };

        vm.welcome = w3Config.get('welcome');
        vm.welcome_text = w3Config.get('welcome_text');
         vm.credits_auth_logo = w3Config.get('credits_auth_logo');
        vm.credits_link = w3Config.get('credits_link');
        vm.bg_auth = w3Config.get('bg_auth');
        vm.logo = w3Config.get('logo');
        vm.showLogo = w3Config.get('showLogo');
        vm.sigla = w3Config.get('sigla');

        // Methods
        vm.login = login;

        //--------------------
        function login() {

            w3Auth.login(vm.credentials).then(function () {
                $state.go(w3Config.get('auth').routeSuccess);
            });

        }

    }
})();
