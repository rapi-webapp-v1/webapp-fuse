(function ()
{
    'use strict';

    angular
        .module('rapi.fuse.auth')
        .controller('ResetPasswordController', ResetPasswordController);

    /** @ngInject */
    function ResetPasswordController(w3Config)
    {
        // Data
        var vm = this;

         vm.bg_auth = w3Config.get('bg_auth');
         vm.logo = w3Config.get('logo');
         vm.sigla = w3Config.get('sigla');

        //////////
    }
})();
