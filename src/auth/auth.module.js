(function () {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
            .module('rapi.fuse.auth', [])
            .config(config);


    /** @ngInject */
    function config($stateProvider) {

        //path folder plugin
        var path = 'app/external/fuse';

        // State definitions
        $stateProvider
                .state('auth', {
                    abstract: true,
                    url: '/auth',
                    views: {
                        'main@': {
                            templateUrl:  path + '/layouts/content/content.html',
                            controller: 'AuthController as vm'
                        }
                    }
                })
                .state('auth.login', {
                    url: '/login',
                    views: {
                        'content': {
                            templateUrl: path + '/auth/login-v2/login-v2.html',
                            controller: 'LoginV2Controller as vm'
                        }
                    },
                    bodyClass: 'login-v2'
                })
                .state('auth.token', {
                    url: '/token?access_token',
                    views: {
                        'content': {
                            controller: 'LoginTokenController'
                        }
                    }
                })
                .state('auth.logout', {
                    url: '/logout',
                    views: {
                        'content': {
                            template: 'Efetuando logout...',
                            controller: 'LogoutController'
                        }
                    }
                })
                .state('auth.reset', {
                    url: '/reset',
                    views: {
                        'content': {
                            templateUrl: path + '/auth/reset-password/reset-password.html',
                            controller: 'ResetPasswordController as vm'
                        }
                    },
                    bodyClass: 'reset-password'
                })
                .state('auth.forgot', {
                    url: '/forgot',
                    views: {
                        'content': {
                            templateUrl: path + '/auth/forgot-password/forgot-password.html',
                            controller: 'ForgotPasswordController as vm'
                        }
                    },
                    bodyClass: 'forgot-password'
                });

//        $stateProvider
//                .state('app.profile', {
//                    url: '/profile',
//                    views: {
//                        'content@app': {
//                            templateUrl: path + '/profile/profile.html',
//                            controller: 'ProfileController as vm'
//                        }
//                    },
//                    bodyClass: 'profile'
//                });

        // Translation
        //$translatePartialLoaderProvider.addPart('/assets/app/external/fuse/components/auth');
    }

})();
