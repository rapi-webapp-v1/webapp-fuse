(function ()
{
    'use strict';

    angular
        .module('rapi.fuse.auth')
        .controller('ForgotPasswordController', ForgotPasswordController);

    /** @ngInject */
    function ForgotPasswordController( w3Config )
    {
             // Data
        var vm = this;

         vm.bg_auth = w3Config.get('bg_auth');
         vm.logo = w3Config.get('logo');
         vm.sigla = w3Config.get('sigla');

        // Methods

        //////////
    }
})();
