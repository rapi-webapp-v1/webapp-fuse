/*!
 * rrule.js - Library for working with recurrence rules for calendar dates.
 * https://github.com/jkbrzt/rrule
 *
 * Copyright 2010, Jakub Roztocil and Lars Schoning
 * Licenced under the BSD licence.
 * https://github.com/jkbrzt/rrule/blob/master/LICENCE
 */


(function (window) {

    'use strict';

    /**
     * Return true if a value is in an array
     */
    var contains = function (arr, val) {
        return arr.indexOf(val) !== -1;
    };

    /**
     * General date-related utilities.
     * Also handles several incompatibilities between JavaScript and Python
     *
     */
    var dateutil = {
        MONTH_DAYS: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        /**
         * Number of milliseconds of one day
         */
        ONE_DAY: 1000 * 60 * 60 * 24,
        /**
         * @see: <http://docs.python.org/library/datetime.html#datetime.MAXYEAR>
         */
        MAXYEAR: 9999,
        /**
         * Python uses 1-Jan-1 as the base for calculating ordinals but we don't
         * want to confuse the JS engine with milliseconds > Number.MAX_NUMBER,
         * therefore we use 1-Jan-1970 instead
         */
        ORDINAL_BASE: new Date(1970, 0, 1),
        /**
         * Python: MO-SU: 0 - 6
         * JS: SU-SAT 0 - 6
         */
        PY_WEEKDAYS: [6, 0, 1, 2, 3, 4, 5],
        /**
         * py_date.timetuple()[7]
         */
        getYearDay: function (date) {
            var dateNoTime = new Date(
                    date.getFullYear(), date.getMonth(), date.getDate())
            return Math.ceil(
                    (dateNoTime - new Date(date.getFullYear(), 0, 1)) / dateutil.ONE_DAY) + 1
        },
        isLeapYear: function (year) {
            if (year instanceof Date)
                year = year.getFullYear()
            return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)
        },
        /**
         * @return {Number} the date's timezone offset in ms
         */
        tzOffset: function (date) {
            return date.getTimezoneOffset() * 60 * 1000
        },
        /**
         * @see: <http://www.mcfedries.com/JavaScript/DaysBetween.asp>
         */
        daysBetween: function (date1, date2) {
            // The number of milliseconds in one day
            // Convert both dates to milliseconds
            var date1_ms = date1.getTime() - dateutil.tzOffset(date1)
            var date2_ms = date2.getTime() - dateutil.tzOffset(date2)
            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            // Convert back to days and return
            return Math.round(difference_ms / dateutil.ONE_DAY)
        },
        /**
         * @see: <http://docs.python.org/library/datetime.html#datetime.date.toordinal>
         */
        toOrdinal: function (date) {
            return dateutil.daysBetween(date, dateutil.ORDINAL_BASE)
        },
        /**
         * @see - <http://docs.python.org/library/datetime.html#datetime.date.fromordinal>
         */
        fromOrdinal: function (ordinal) {
            var millisecsFromBase = ordinal * dateutil.ONE_DAY
            return new Date(dateutil.ORDINAL_BASE.getTime() -
                    dateutil.tzOffset(dateutil.ORDINAL_BASE) +
                    millisecsFromBase +
                    dateutil.tzOffset(new Date(millisecsFromBase)))
        },
        /**
         * @see: <http://docs.python.org/library/calendar.html#calendar.monthrange>
         */
        monthRange: function (year, month) {
            var date = new Date(year, month, 1)
            return [dateutil.getWeekday(date), dateutil.getMonthDays(date)]
        },
        getMonthDays: function (date) {
            var month = date.getMonth()
            return month === 1 && dateutil.isLeapYear(date)
                    ? 29 : dateutil.MONTH_DAYS[month]
        },
        /**
         * @return {Number} python-like weekday
         */
        getWeekday: function (date) {
            return dateutil.PY_WEEKDAYS[date.getDay()]
        },
        /**
         * @see: <http://docs.python.org/library/datetime.html#datetime.datetime.combine>
         */
        combine: function (date, time) {
            time = time || date
            return new Date(
                    date.getFullYear(), date.getMonth(), date.getDate(),
                    time.getHours(), time.getMinutes(), time.getSeconds(),
                    time.getMilliseconds())
        },
        clone: function (date) {
            var dolly = new Date(date.getTime())
            return dolly
        },
        cloneDates: function (dates) {
            var clones = []
            for (var i = 0; i < dates.length; i++) {
                clones.push(dateutil.clone(dates[i]))
            }
            return clones
        },
        /**
         * Sorts an array of Date or dateutil.Time objects
         */
        sort: function (dates) {
            dates.sort(function (a, b) {
                return a.getTime() - b.getTime()
            })
        },
        timeToUntilString: function (time) {
            var comp

            var date = new Date(time);

            var comps = [
                date.getUTCFullYear(),
                date.getUTCMonth() + 1,
                date.getUTCDate(),
                'T',
                date.getUTCHours(),
                date.getUTCMinutes(),
                date.getUTCSeconds(),
                'Z'
            ]

            for (var i = 0; i < comps.length; i++) {
                comp = comps[i]
                if (!/[TZ]/.test(comp) && comp < 10)
                    comps[i] = '0' + String(comp)
            }
            return comps.join('')
        },
        untilStringToDate: function (until) {
            var re = /^(\d{4})(\d{2})(\d{2})(T(\d{2})(\d{2})(\d{2})Z)?$/
            var bits = re.exec(until)
            if (!bits)
                throw new Error('Invalid UNTIL value: ' + until)
            return new Date(Date.UTC(
                    bits[1],
                    bits[2] - 1,
                    bits[3],
                    bits[5] || 0,
                    bits[6] || 0,
                    bits[7] || 0))
        }
    }

    dateutil.Time = function (hour, minute, second, millisecond) {
        this.hour = hour
        this.minute = minute
        this.second = second
        this.millisecond = millisecond || 0
    }

    dateutil.Time.prototype = {
        constructor: dateutil.Time,
        getHours: function () {
            return this.hour
        },
        getMinutes: function () {
            return this.minute
        },
        getSeconds: function () {
            return this.second
        },
        getMilliseconds: function () {
            return this.millisecond
        },
        getTime: function () {
            return ((this.hour * 60 * 60) + (this.minute * 60) + this.second) * 1000 +
                    this.millisecond
        }
    }

    var RRule = function (conf) {
        conf = conf || {};

        // used by toString()
        this.options = {};
        this.origOptions = {};

        var invalid = [];
        var keys = Object.keys(conf);
        var defaultKeys = Object.keys(RRule.DEFAULT_OPTIONS);

        // Shallow copy for origOptions and check for invalid
        keys.forEach(function (key) {
            this.origOptions[key] = conf[key];
            if (!contains(defaultKeys, key))
                invalid.push(key);
        }, this);

        if (invalid.length)
            throw new Error('Invalid options: ' + invalid.join(', '));

        if (!RRule.FREQUENCIES[conf.freq] && conf.byeaster === null) {
            throw new Error('Invalid frequency: ' + String(conf.freq));
        }

        // Merge in default options
        defaultKeys.forEach(function (key) {
            if (!contains(keys, key))
                conf[key] = RRule.DEFAULT_OPTIONS[key];
        });

        this.options = conf;
    };


// RRule class 'constants'

    RRule.FREQUENCIES = [
        'YEARLY', 'MONTHLY', 'WEEKLY', 'DAILY',
        'HOURLY', 'MINUTELY', 'SECONDLY'
    ];

    RRule.YEARLY = 0;
    RRule.MONTHLY = 1;
    RRule.WEEKLY = 2;
    RRule.DAILY = 3;
    RRule.HOURLY = 4;
    RRule.MINUTELY = 5;
    RRule.SECONDLY = 6;

    RRule.MO = new Weekday(0);
    RRule.TU = new Weekday(1);
    RRule.WE = new Weekday(2);
    RRule.TH = new Weekday(3);
    RRule.FR = new Weekday(4);
    RRule.SA = new Weekday(5);
    RRule.SU = new Weekday(6);

    RRule.DEFAULT_OPTIONS = {
        freq: null,
        dtstart: null,
        interval: 1,
        wkst: RRule.MO,
        count: null,
        until: null,
        bysetpos: null,
        bymonth: null,
        bymonthday: null,
        bynmonthday: null,
        byyearday: null,
        byweekno: null,
        byweekday: null,
        bynweekday: null,
        byhour: null,
        byminute: null,
        bysecond: null,
        byeaster: null
    };

    RRule.optionsToString = function (options) {
        var key, value, strValues;
        var pairs = []
        var keys = Object.keys(options)
        var defaultKeys = Object.keys(RRule.DEFAULT_OPTIONS)

        for (var i = 0; i < keys.length; i++) {
            if (!contains(defaultKeys, keys[i]))
                continue

            key = keys[i].toUpperCase()
            value = options[keys[i]]
            strValues = []

            if (value === null || value instanceof Array && !value.length)
                continue

            switch (key) {
                case 'FREQ':
                    value = RRule.FREQUENCIES[options.freq]
                    break
                case 'WKST':
                    value = value.toString()
                    break
                case 'BYWEEKDAY':
                    /*
                     NOTE: BYWEEKDAY is a special case.
                     RRule() deconstructs the rule.options.byweekday array
                     into an array of Weekday arguments.
                     On the other hand, rule.origOptions is an array of Weekdays.
                     We need to handle both cases here.
                     It might be worth change RRule to keep the Weekdays.

                     Also, BYWEEKDAY (used by RRule) vs. BYDAY (RFC)

                     */
                    key = 'BYDAY'
                    if (!(value instanceof Array))
                        value = [value]

                    for (var wday, j = 0; j < value.length; j++) {
                        wday = value[j]
                        if (wday instanceof Weekday) {
                            // good
                        } else if (wday instanceof Array) {
                            wday = new Weekday(wday[0], wday[1])
                        } else {
                            wday = new Weekday(wday)
                        }
                        strValues[j] = wday.toString()
                    }
                    value = strValues
                    break
                case 'DTSTART':                    
                    value = dateutil.timeToUntilString(value)
                    break
                case 'UNTIL':
                    value.addDays(1); //Adiciona mais um dia para que o sistema possa contabilizar o dia ainda
                    value = dateutil.timeToUntilString(value)
                    break
                default:
                    if (value instanceof Array) {
                        for (j = 0; j < value.length; j++)
                            strValues[j] = String(value[j])
                        value = strValues
                    } else {
                        value = String(value)
                    }

            }
            pairs.push([key, value])
        }

        var strings = []
        for (i = 0; i < pairs.length; i++) {
            var attr = pairs[i]
            strings.push(attr[0] + '=' + attr[1].toString())
        }
        return strings.join(';')
    };

    RRule.prototype = {
        constructor: RRule,
        /**
         * Converts the rrule into its string representation
         * @see <http://www.ietf.org/rfc/rfc2445.txt>
         * @return String
         */
        toString: function () {
            return RRule.optionsToString(this.origOptions)
        }
    };
//// WEEK
    function Weekday(weekday, n) {
        if (n === 0)
            throw new Error("Can't create weekday with n == 0")
        this.weekday = weekday
        this.n = n
    }

    Weekday.prototype = {
        constructor: Weekday,
        // __call__ - Cannot call the object directly, do it through
        // e.g. RRule.TH.nth(-1) instead,
        nth: function (n) {
            return this.n === n ? this : new Weekday(this.weekday, n)
        },
        // __eq__
        equals: function (other) {
            return this.weekday === other.weekday && this.n === other.n
        },
        // __repr__
        toString: function () {
            var s = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'][this.weekday]
            if (this.n)
                s = (this.n > 0 ? '+' : '') + String(this.n) + s
            return s
        },
        getJsWeekday: function () {
            return this.weekday === 6 ? 0 : this.weekday + 1
        }

    }


    window.RRule = RRule;

})(window);

///////////////////
//var vm = {
//    row: {
//        frequencia: RRule.MONTHLY,
//        dt_prevista: new Date(),
//        frequencia_fim: new Date()
//    }
//};
//
//var rule = new RRule({
//    freq: vm.row.frequencia,
//    interval: 1,
//    dtstart: vm.row.dt_prevista,
//    until: vm.row.frequencia_fim
//});
//
//var str = rule.toString();
//
//
//console.log('RULE', str);
