(function () {
    'use strict';
    
    angular
            .module('rapi.fuse')
            .directive('rfActivities', activitiesDirective)
            .controller('rfActivitiesController', activitiesController);

            
    /**
     * USAGE:
     * <rf-activities rf-token="vm.row.token_morph.token"></rf-activities>
     *
     * @constructor
     */
    function activitiesDirective() {
        return {
            restrict: 'E',
            scope: {
                token: "=rfToken"
            },
            controller: 'rfActivitiesController as vm',
            templateUrl: 'app/external/fuse/components/rf-activities/activities.tmpl.html'
        };
    }

    /** @ngInject */
    function activitiesController($scope, $http, rfUtils)
    {
        var vm = this;
        
        //Data
        vm.activities = [];

        //--------------------------------
        $scope.$watch('token', activate);

        function activate()
        {
            if ($scope.token) {
                loadLogs();
            }

            $scope.$on('rfActivities.reload', function(){// este callback envia um paramentro event
                //event.preventDefault();//myFeature implements pusher socket io
                loadLogs();
            });
        }

        function loadLogs() {

            var data = {
                params: {
                    'token_morph': $scope.token,
                    'include': 'causer',
                    'sort': '-created_at',
                    'take': -1 //mytodo paginar result (colocar btn de ler mais) page++
                }
            };

            var url = rfUtils.urlApi('/rapi/activities');

            $http.get(url, data).then(function (result) {
                vm.activities = result.data.data;
            });
        }

    }


})();
