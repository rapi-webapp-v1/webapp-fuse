(function () {
    'use strict';

    angular
            .module('rapi.fuse')
            .directive('rfComments', Comments)
            .controller('rfCommentsController', rfCommentsController);

    /**
     * USAGE:
     * <rf-comments rf-token="vm.row.token_morph.token"></rf-comments>
     *
     * @constructor
     */
    function Comments() {
        return {
            restrict: 'E',
            scope: {
                token: "=rfToken"
            },
            controller: 'rfCommentsController as vm',
            templateUrl: 'app/external/fuse/components/rf-comments/comments.tmpl.html'
        };
    }

    /** @ngInject */
    function rfCommentsController($scope, RapiComment) {

        var vm = this;

        vm.rowComment = {
            'token_morph': null,
            'message': '',
            'type': 'mensagem',
            'include': 'profile'
        };
        vm.comments = [];

        /*  methods */
        vm.addNewComment = addNewComment;

        /////////////////////////////////////////////
        $scope.$watch('token', activate);

        function activate()
        {
            if ($scope.token) {
                loadMessages();
            }
        }

        function loadMessages() {
            var data = {
                'token_morph': $scope.token,
                'include': 'profile',
                'sort': '-created_at'
            };

            RapiComment.search(data).then(function (result) {
                vm.comments = result;
            });
        }

        /**
         * Add new comment
         *
         * @param newCommentText
         */
        function addNewComment() {
            
            var data = angular.copy(vm.rowComment);
            data.token_morph = $scope.token;
            
            RapiComment.save(data).then(function (result) {
                vm.comments = [result].concat(vm.comments);
                vm.rowComment.message = '';
            });

        }

    }

})();
