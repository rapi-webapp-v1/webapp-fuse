(function () {
    'use strict';

    angular
            .module('rapi.fuse')
            .factory('RapiComment', RapiCommentService);

     //mytodo isso deveria estar no fuse
    /** @ngInject */
    function RapiCommentService(w3Resource) {
        var urlApi = "/rapi/comments";
        var service = {
            name: 'comment'
        };
        return w3Resource.extend(urlApi, service);
    }
})();