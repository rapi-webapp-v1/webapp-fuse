(function () {
    'use strict';

    angular
            .module('rapi.fuse')
            .directive('rfAvatar', Avatar);


    /**
     * USAGE:
     * <rf-avatar  rf-size="80" rf-profile="vm.perfil" rf-only-text="true" ></rf-avatar>
     * rf-size *5 <= 500 OR *8<=800
     * rf-only-text boolean
     * rf-profile objeto com dados do usuário
     * @constructor
     */

    /** @ngInject */
    function Avatar($filter) {
        return {
            restrict: 'EA',
            scope: {
                c: "=rfProfile",
                size: "=rfSize",
                onlyText: "=rfOnlyText",
                disable: "=?",
                presenter: "@w3Presenter"
            },
            link: function (scope) {

                scope.classColor = 'grey-400-bg';
                scope.profile = {
                    sex: null,
                    name: '',
                    thumb: null
                };
             
               
                                
                scope.$watch('c', build);

                function build(newProfile) {
                   
                    
                    if (!newProfile) {
                        return;
                    } 
                    
                    if (scope.presenter) {
                        scope.profile = angular.copy($filter(scope.presenter)(newProfile));
                    } else {
                        scope.profile = newProfile;
                    }

                    scope.classColor = 'grey-400-bg';

                    if (scope.profile.sex === "female" || scope.profile.sex === "F" ) {
                        scope.classColor = 'pink-400-bg';
                    } else if (scope.profile.sex === "male" || scope.profile.sex === "M") {
                        scope.classColor = 'md-blue-400-bg';
                    }
                    
                     scope.mode = (scope.profile.thumb && !scope.onlyText)? 'img' : 'text';
              
                }

            },
            templateUrl: 'app/external/fuse/components/rf-avatar/rf-avatar.tmpl.html'
        };
    }




})();
