(function () {
    'use strict';

    angular
            .module('rapi.fuse')
            .controller('rfInputCityController', rfInputCityController)
            .directive('rfInputCity', rfInputCityDirective);

    /**
     * EX: <rf-input-city ng-model="vm.row.city_id" rf-city="vm.row.city" rf-open-create="false" flex-gt-xs="75"></rf-input-city>
     *
     * */
    /** @ngInject */
    function rfInputCityDirective() {
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                ngModel: '=',
                city: '=rfCity',
                placeholder: '=rfPlaceholder',
                openCreate: '@rfOpenCreate'
            },
            templateUrl: 'app/external/fuse/components/rf-input-city/rf-input-city.tpl.html',
            controller: 'rfInputCityController as vm'
        };
    }

    /** @ngInject */
    function rfInputCityController($scope, City, $log) {
        var vm = this;

        // Data
        vm.archive = [];

        // Methods
        vm.querySearch = querySearch;
        vm.selectedItemChange = selectedItemChange;
        vm.showCreateCity = showCreateCity;

        activate();
        //-----------------------
        $scope.$watch('city', setCity);
        $scope.$watch('placeholder', setCity);

        function setCity(obj)
        {
            $log.debug('rfInputCity:$watch', obj);

            if (!obj) {
                vm.selectedItem = null;
            } else if (angular.isString(obj)) {
               
                vm.selectedItem = {city_name: obj, id: false};
            } else if (angular.isObject(obj)) {
                var name = obj.city_name;
                name += (obj.state) ? ", " + obj.state.state_code : '';
                name += (obj.state.country) ? ' - ' + obj.state.country.country_code : '';
                vm.selectedItem = {'city_name': name, id: obj.id};
            }
        }

        function activate()
        {
            vm.openCreate = ($scope.openCreate === 'false') ? false : true;
        }

        function querySearch(query)
        {
            return City.search({
                'q': query,
                'take': 5,
                'sort': 'city_name',
                'include': 'state.country'
            });
        }

        function selectedItemChange(item)
        {
            $scope.$evalAsync(function () {
                if (item.id !== false)
                    $scope.ngModel = (item) ? item.id : null;
            });
        }

        function showCreateCity(ev)
        {
            var data = {
                'city_name': vm.searchText,
                'include': 'state.country'
            };

            City.dialog.create([], ev, data).then(function (result) {
                if (result)
                    setCity(result);

            });
        }

    }

})();
