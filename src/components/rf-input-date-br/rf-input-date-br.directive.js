(function () {

    angular
            .module('rapi.fuse')
            .directive('rfInputDateBr', rfInputDateBrDirective);

    /**
     * Gera input text com mascara de data BR
     * Faz bind com model, para retornar um obj Date
     * 
     * @usage <rf-input-date-br rf-model="vm.row.data_nasc" rf-required="true"></rf-input-date-br>
     */

    /** @ngInject */
    function rfInputDateBrDirective(w3Date) {

        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: '<input ng-model="date" mask="{{$root.fmask.data}}" type="text" ng-required="{{rf.required}}" />',
            controllerAs: 'rf',
            scope: {
                model: '=rfModel',
                required: '=rfRequired'
            }
        };

        return directive;

        function link(scope, element) {

            scope.$watch('model', function (novo) {
                if (novo) {
                    scope.date = w3Date.dateToPt(novo);
                }
            });

            element.on('blur', function () {
                scope.model = w3Date.ptToDate(element.val());
            });

        }
    }

})();