(function () {
    'use strict';

    angular
            .module('rapi.fuse.xsession')
            .provider('xSession', xSessionProvider);


    /** @ngInject */
    function xSessionProvider() {

        var self = this;
        var HAS_SESSION = false;

        var CONFIG = {
            home: 'app.xsession',
            keyHeader: 'X-SESSION',
            nameToolbar: 'App',
            labels: {
                title: 'Olá,',
                content: 'Nós percebemos que você têm acesso a mais de uma unidade, com qual delas deseja trabalhar agora?'
            },
            queryModel: null
        };

        this.setDefaultID = function (ID)
        {
            window.sessionStorage.setItem(CONFIG['keyStorangeID'], ID);
        };

        this.getDefaultID = function ()
        {
            return window.sessionStorage.getItem(CONFIG['keyStorangeID']);
        };

        this.configure = function (config)
        {
            CONFIG = angular.extend(CONFIG, config);
            CONFIG['keyStorange'] = CONFIG['keyHeader'].replace('-', '_').toLowerCase();
            CONFIG['keyStorangeID'] = CONFIG['keyStorange'] + '_id';
            console.log(CONFIG);
        };

        this.setQueryModel = function (query)
        {
            CONFIG['queryModel'] = query;
        };

        this.$get = XSession;

        /** @ngInject */
        function XSession($injector, $log, $rootScope, w3Session) {

            var service = {
                setDefaultID: self.setDefaultID,
                ID: getID,
                getModel: getModel,
                setModel: setModel,
                unsetModel: unsetModel,
                guest: guest,
                check: check,
                stateHome: getNameStateHome,
                searchModel: searchModel,
                labels: labels,
                getConfig: getConfig
            };

            activate();
            return service;

            //------------------------------
            function activate()
            {
                HAS_SESSION = (getID()) ? true : false;
                updateRootScope();
            }

            function updateRootScope()
            {
                $rootScope.currentXSession = getModel();
                $rootScope.nameToolbar = getConfig('nameToolbar');
            }

            function getConfig(key)
            {
                return (key) ? CONFIG[key] : CONFIG;
            }

            function getNameStateHome()
            {
                return CONFIG['home'];
            }

            function check()
            {
                return (HAS_SESSION === true);
            }

            function guest()
            {
                return !check();
            }

            function setID(id)
            {

            }

            function getID()
            {
                var model = getModel();
                return (model) ? model.id : null;
            }

            function getModel()
            {
                var model = w3Session.get(CONFIG['keyStorange']);

                if (!model) {
                    $log.error('XSession:getModel -> Undefined X-SESSION');
                    $rootScope.$broadcast('XSession:undefined');
                    return false;
                }

                return model;
            }

            function unsetModel()
            {
                var model = {id: 0, nome: 'Não informado'};
                setModel(model);
            }

            function setModel(model)
            {
                var _model = {
                    id: model.id,
                    nome: model.nome
                };

                console.log('XSession:setModel -> ', _model);

                w3Session.set(CONFIG['keyStorange'], _model);
                self.setDefaultID(_model.id);
                onChangeModel(_model);
            }

            function onChangeModel(model)
            {
                activate();
                $rootScope.$broadcast('XSession:changed', model);
            }

            function searchModel()
            {
                if (CONFIG['queryModel']) {
                    return  $injector.invoke(CONFIG['queryModel']);
                } else {
                    console.error('xSession: queryModel não definido');
                }
            }

            function labels()
            {
                return CONFIG['labels'];
            }



        }


    }

})();
