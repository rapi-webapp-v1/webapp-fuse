(function () {
    'use strict';

    angular
            .module('rapi.fuse.xsession')
            .controller('XSessionHomeController', xSessionHomeController)
            .controller('XSessionController', xSessionController);

    /** @ngInject */
    function xSessionHomeController($state, xSession) {

        var name = xSession.getNameStateHome();

        $state.go(name);

    }

    /** @ngInject */
    function xSessionController($state, xSession) {

        var vm = this;

        // Data
        vm.archive = [];
        vm.labels = xSession.labels();


        // Methods
        vm.selectModel = selectModel;

        activate();

        //-----------------------

        function activate()
        {
            xSession.unsetModel();
            loadModels();
        }

        function selectModel(model)
        {
            xSession.setModel(model);
            $state.go(xSession.stateHome(), {'XID': xSession.ID()});
        }

        function loadModels()
        {
            xSession.searchModel().then(function (result) {
                vm.archive = result;

                if (vm.archive.length === 1) {
                    selectModel(vm.archive[0]);
                }
            });
        }

    }



})();
