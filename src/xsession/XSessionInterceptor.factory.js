(function() {
    'use strict';

    angular
        .module('rapi.fuse.xsession')
        .factory('XSessionInterceptor', xSessionInterceptor);


    /** @ngInject */
    function xSessionInterceptor(xSession) {

        var urlPrefix = 'api/v1';

        var service = {
            request: customRequest
        };

        return service;
        //------------------------------

        /**
         * Custom HTTP on Request do something on success
         */
        function customRequest(request)
        {           
            //Caso a URL não seja de api nem add UNIDADE ID
            if (request.url.indexOf(urlPrefix) < 0) {
                //console.log('XSessionInterceptor:ignoreURL:', request.url);
                return request;
            }
            
            var key = xSession.getConfig('keyHeader');

            request.headers = request.headers || {};
            request.headers[key] = xSession.ID();
            
            console.log('XSessionInterceptor:request', 'HEADER-KEY:' + key + '->', request.headers[key], request.url);

            return request;
        }

    }

})();
