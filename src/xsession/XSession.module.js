(function () {
    'use strict';

    angular
            .module('rapi.fuse.xsession', [])
            .config(config)
            .run(run);

    /** @ngInject */
    function config($httpProvider, $urlRouterProvider, $stateProvider, xSessionProvider)
    {
        $urlRouterProvider.otherwise('/session/home');
        $httpProvider.interceptors.push('XSessionInterceptor');

        // State
        $stateProvider
                .state('app.xsession', {
                    url: '/session',
                    views: {
                        'content@app': {
                            templateUrl: 'app/external/fuse/xsession/list-sessions.html',
                            controller: 'XSessionController as vm'
                        }
                    }
                })
                .state('app.xsession_home', {
                    url: '/session/home',
                    views: {
                        'content@app': {
                            controller: 'XSessionHomeController as vm'
                        }
                    }
                });

        //////X SESSION
        $stateProvider.state('xsession', {
            url: '/x/{XID}',
            abstract: true,
            parent: 'app',
            requiredXSession: true,
            params: {
                XID: {
                    value: xSessionProvider.getDefaultID()
                }
            },
            views: {
                'toolbar@app': {
                    templateUrl: 'app/external/fuse/xsession/toolbar.html',
                    controller: 'ToolbarController as vm'
                }
            }
        });

    }

    /** @ngInject */
    function run($rootScope, $log, $state, xSession)
    {

        $rootScope.$on('XSession:undefined', function () {
            $state.go('app.xsession');
        });
        
        //xSession.setDefaultID()

        $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {

                    if ((toState.checkSessionRequest || toState.name.indexOf('xsession') === 0) && xSession.guest()) {
                        event.preventDefault();
                        $log.warn('$checkSessionRequest->', toState.name);
                        $state.go('app.xsession');
                    }

                });
    }

})();