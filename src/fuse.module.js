(function () {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
            .module('rapi.fuse', [
                //'rapi.fuse.layouts',
                'rapi.fuse.auth',
                'rapi.w3',

                        //MODULES AVALABLES
                        'rapi.fuse.profile'
                        //'rapi.fuse.cities'
            ])


            /** @ngInject */
            .run(function ($rootScope, $window, $state, $timeout, w3Config, w3Auth) {

                $rootScope.rapiProfile = w3Auth.profile();

                $rootScope.$on('w3Auth:loginSuccess', function () {
                    $rootScope.rapiProfile = w3Auth.profile();
                });

                $rootScope.$on('w3Auth:logout', function () {
                    $rootScope.rapiProfile = null;
                });

                $rootScope.$on('w3HttpInterceptor:loginRequired', function () {
                    var callback = w3Config.get('auth').onLoginRequired;
                     
                    if (angular.isFunction(callback)) {
                        console.log('callback', callback);
                        callback();
                    }
                });

                // 3rd Party Dependencies
//                editableThemes.default.submitTpl = '<md-button class="md-icon-button" type="submit" aria-label="save"><md-icon md-font-icon="icon-checkbox-marked-circle" class="md-accent-fg md-hue-1"></md-icon></md-button>';
//                editableThemes.default.cancelTpl = '<md-button class="md-icon-button" ng-click="$form.$cancel()" aria-label="cancel"><md-icon md-font-icon="icon-close-circle" class="icon-cancel"></md-icon></md-button>';

                // Activate loading indicator
                var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function (event, toState)//event, toState, toParams, fromState, fromParams
                {
                    $rootScope.loadingProgress = true;
                    //mytodo configure options state [checkAuth|noAuth] ..
                    if (toState['external']) {
                        event.preventDefault();
                        $window.open(toState.url, '_blank');
                        $rootScope.loadingProgress = false;
                    }

                });

                // De-activate loading indicator
                var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
                {
                    $timeout(function ()
                    {
                        $rootScope.loadingProgress = false;
                    });
                });

                // Store state in the root scope for easy access
                $rootScope.state = $state;

                // Cleanup
                $rootScope.$on('$destroy', function ()
                {
                    stateChangeStartEvent();
                    stateChangeSuccessEvent();
                });

                $rootScope.fmask = {
                    data: '39/19/9999', //data PT
                    cpf: '999.999.999-99',
                    rg: '99.999.999-9?9',
                    cnpj: '99.999.999/9999-99',
                    tel: '(99) 9?9999-9999',
                    //tel     : '(99) 9999-9999',
                    cel: '(99) 9999-9999',
                    cep: '99999-999',
                    porcentage: '1?9?9?.99?9?'
                };

    
            });

})();
