(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('rapi.fuse.layouts.vert_nav', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        var path = 'app/external/fuse/layouts';  

        // State definitions
        $stateProvider
                .state('app', {
                    abstract: true,
                    views: {
                        'main@': {
                            templateUrl: path + '/vert-nav/vert-nav.html',
                            controller: 'LayoutsController as vm'
                        },
                        'navigation@app': {
                            templateUrl:  path + '/partials/navigation/vertical-navigation.html',
                            controller: 'NavigationController as vm'
                        }
                    }
                });

 msNavigationServiceProvider.saveItem('logout', {
            title: 'COMMON.SAIR',
            icon: 'icon-logout',
            state: 'auth.logout',
            weight: 999
        });
   
    }


})();
