(function ()
{
    'use strict';

    angular
        .module('rapi.fuse.layouts')
        .controller('QuickPanelController', QuickPanelController);

    /** @ngInject */
    function QuickPanelController()
    {
        var vm = this;

        // Data
     
        vm.settings = {
            notify: true,
            cloud : false,
            retro : true
        };
        vm.activities = [
            {
                created_at: '12/12/2017',
                description: 'Atividade x em dd',
                link: '',
                profile: {
                    name: 'Pessoa A'
                }
            },
            {
                created_at: '12/12/2017',
                description: 'Atividade x em dd',
                link: '',
                profile: {
                    name: 'Pessoa A'
                }
            }
        ];

        // Methods

        //////////
    }

})();