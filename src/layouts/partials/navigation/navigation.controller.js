(function() {
    'use strict';

    angular
        .module('rapi.fuse.layouts')
        .controller('NavigationController', NavigationController);

    /** @ngInject */
    function NavigationController($scope, w3Config) {
        var vm = this;

        // Data
        vm.bodyEl = angular.element('body');
        vm.folded = false;
        vm.msScrollOptions = {
            suppressScrollX: true
        };
        vm.logo = w3Config.get('logo_painel');
        vm.credits_logo = w3Config.get('credits_logo');
        vm.credits_link = w3Config.get('credits_link');
        vm.versao = w3Config.get('versao');
        vm.sigla = w3Config.get('sigla');
        vm.title_project = w3Config.get('title_project');
        
        // Methods
        vm.toggleMsNavigationFolded = toggleMsNavigationFolded;

        //////////

        /**
         * Toggle folded status
         */
        function toggleMsNavigationFolded() {
         
            vm.folded = !vm.folded;
      
        }

        // Close the mobile menu on $stateChangeSuccess
        $scope.$on('$stateChangeSuccess', function() {
            vm.bodyEl.removeClass('ms-navigation-horizontal-mobile-menu-active');
        });
    }

})();
