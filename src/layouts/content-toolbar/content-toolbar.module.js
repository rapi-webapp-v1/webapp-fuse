(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
            .module('rapi.fuse.layouts.content_toolbar', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        var path = 'app/external/fuse/layouts';

        // State definitions
        $stateProvider
                .state('app', {
                    abstract: true,
                    views: {
                        'main@': {
                            templateUrl: path + '/content-toolbar/content-toolbar.html',
                            controller: 'LayoutsController as vm'
                        },
                        'toolbar@app': {
                            templateUrl: path + '/partials/toolbar/hor-toolbar.html',
                            controller: 'ToolbarController as vm'
                        },
                         'navigation@app': {
                            templateUrl:  path + '/partials/navigation/horizontal-navigation.html',
                            controller: 'NavigationController as vm'
                        },
                        'quickPanel@app': {
                            templateUrl: path + '/partials/quickpanel/quickpanel.html',
                            controller: 'QuickPanelController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('logout', {
            title: 'COMMON.SAIR',
            icon: 'icon-logout',
            state: 'auth.logout',
            weight: 999
        });

    }


})();
