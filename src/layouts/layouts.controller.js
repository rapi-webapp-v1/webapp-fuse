(function ()
{
    'use strict';

    angular
        .module('rapi.fuse.layouts')
        .controller('LayoutsController', LayoutsController);

  /** @ngInject */
  function LayoutsController($scope, $rootScope, w3Config)
  {
     var vm = this;
      // Data

      //////////
      // Data
      // vm.bg_auth = w3Config.get('bg_auth');
      // vm.logo = w3Config.get('logo');
      // vm.sigla = w3Config.get('sigla');
      // vm.title_project = w3Config.get('title_project');
       vm.openMenu = w3Config.get('menuOpenFrom');

      // Remove the splash screen
      $scope.$on('$viewContentAnimationEnded', function (event)
      {
          if ( event.targetScope.$id === $scope.$id )
          {
              $rootScope.$broadcast('msSplashScreen::remove');
          }
      });
  }


})();
