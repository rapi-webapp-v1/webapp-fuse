(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
            .module('rapi.fuse.layouts.vert_nav_comp_toolbar2', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        var path = 'app/external/fuse/layouts';

        // State definitions
        $stateProvider
                .state('app', {
                    abstract: true,
                    views: {
                        'main@': {
                            templateUrl: path + '/vert-nav-comp-toolbar2/vert-nav-comp-toolbar2.html',
                            controller: 'LayoutsController as vm'
                        },
                        'toolbar@app': {
                            templateUrl: path + '/partials/toolbar/toolbar2.html',
                            controller: 'ToolbarController as vm'
                        },
                         'navigation@app': {
                            templateUrl:  path + '/partials/navigation/vertical-navigation2.html',
                            controller: 'NavigationController as vm'
                        },
                        'quickPanel@app': {
                            templateUrl: path + '/partials/quickpanel/quickpanel.html',
                            controller: 'QuickPanelController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('logout', {
            title: 'COMMON.SAIR',
            icon: 'icon-logout',
            state: 'auth.logout',
            weight: 999
        });

    }


})();
