## Ng Mask

Por padrão algumas máscaras do ng-mask vem configuradas no projeto deste pluguin, 
caso queira utilizálas basta adicionar o ng mask ao bower.jason do seu módulo e 
chamar a dependência no arquivo de criação do módulo. 

* Resumo *

- Install the dependency:
```
bower install angular-mask --save
```

- Add ngMask.min.js to your code:
```
<script src='bower_components/ngMask/dist/ngMask.min.js'></script>
```

- Include module dependency:
```
angular.module('yourApp', ['ngMask']);
```


As mascaras já ativas são:  
- phone [(45) 0000-0000]  
- cep [00.000-000]  
- cpf [000.000000-00]  
- cnpj [52.376.524/0001-85]  

```
input mask="{{$root.fmask.cpf}}" 
```

 Mais informações obre ng-mask em: https://github.com/candreoliveira/ngMask