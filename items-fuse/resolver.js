/** @ngInject */
function config($translateProvider)
{
    // Put your common app configurations here

    // angular-translate configuration
//    $translateProvider.useLoader('$translatePartialLoader', {
//        urlTemplate: '{part}/i18n/{lang}.json'
//    });
//    $translateProvider.preferredLanguage('en');
//    $translateProvider.useSanitizeValueStrategy('sanitize');
}

/** @ngInject */
function runBlock($rootScope, $timeout, $state)
{
    // Activate loading indicator
//    var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function ()
//    {
//        $rootScope.loadingProgress = true;
//    });

    // De-activate loading indicator
//    var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
//    {
//        $timeout(function ()
//        {
//            $rootScope.loadingProgress = false;
//        });
//    });
//
//    // Store state in the root scope for easy access
//    $rootScope.state = $state;
//
//    // Cleanup
//    $rootScope.$on('$destroy', function ()
//    {
//        stateChangeStartEvent();
//        stateChangeSuccessEvent();
//    });
}



//.controller('MainController', MainController);

/** @ngInject */
function MainController($scope, $rootScope)
{
    // Data

    //////////

    // Remove the splash screen
    $scope.$on('$viewContentAnimationEnded', function (event)
    {
        if (event.targetScope.$id === $scope.$id)
        {
            $rootScope.$broadcast('msSplashScreen::remove');
        }
    });
}